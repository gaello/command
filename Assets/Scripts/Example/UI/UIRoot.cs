﻿using UnityEngine;

/// <summary>
/// UI root class which holds references to the different views.
/// </summary>
public class UIRoot : MonoBehaviour
{
    [SerializeField]
    private UICollectCommandsView collectCommandsView;
    public UICollectCommandsView CollectCommandsView => collectCommandsView;

    [SerializeField]
    private UIExecutionSummaryView executionSummaryView;
    public UIExecutionSummaryView ExecutionSummaryView => executionSummaryView;
}
