﻿using UnityEngine;
using UnityEngine.Events;
using TMPro;

/// <summary>
/// Collecting Command view with events for buttons and showing command list.
/// </summary>
public class UICollectCommandsView : UIView
{
    // Reference to the label with command list.
    [SerializeField]
    private TextMeshProUGUI label;

    public override void ShowView()
    {
        base.ShowView();

        // Clear command list label when showing view.
        label.text = "Command List:";
    }

    // Adding new command to the command list label.
    public void AddCommand(string command)
    {
        label.text += "\n" + command;
    }

    // Event called when Execute Button is clicked.
    public UnityAction OnExecuteClicked;

    /// <summary>
    /// Method called by Execute Button.
    /// </summary>
    public void ExecuteClick()
    {
        OnExecuteClicked?.Invoke();
    }

    // Event called when Up Button is clicked.
    public UnityAction OnUpClicked;

    /// <summary>
    /// Method called by Up Button.
    /// </summary>
    public void UpClick()
    {
        OnUpClicked?.Invoke();
    }

    // Event called when Down Button is clicked.
    public UnityAction OnDownClicked;

    /// <summary>
    /// Method called by Down Button.
    /// </summary>
    public void DownClick()
    {
        OnDownClicked?.Invoke();
    }

    // Event called when Left Button is clicked.
    public UnityAction OnLeftClicked;

    /// <summary>
    /// Method called by Left Button.
    /// </summary>
    public void LeftClick()
    {
        OnLeftClicked?.Invoke();
    }

    // Event called when Right Button is clicked.
    public UnityAction OnRightClicked;

    /// <summary>
    /// Method called by Right Button.
    /// </summary>
    public void RightClick()
    {
        OnRightClicked?.Invoke();
    }
}
