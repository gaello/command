﻿using UnityEngine;
using UnityEngine.Events;
using TMPro;

/// <summary>
/// Execution Summary view with events for buttons and showing outcome message.
/// </summary>
public class UIExecutionSummaryView : UIView
{
    // Reference to the outcome message label.
    [SerializeField]
    private TextMeshProUGUI label;

    // Message when player won.
    [SerializeField]
    private string wonMessage;
    // Messege when player lost.
    [SerializeField]
    private string loseMessage;

    /// <summary>
    /// Method which display message based on game outcome.
    /// </summary>
    /// <param name="gameWon">If set to <c>true</c> game won.</param>
    public void ShowMessage(bool gameWon)
    {
        label.text = gameWon ? wonMessage : loseMessage;
    }

    // Event called when Replay Button is clicked.
    public UnityAction OnReplayClicked;

    /// <summary>
    /// Method called by Replay Button.
    /// </summary>
    public void ReplayClick()
    {
        OnReplayClicked?.Invoke();
    }
}
