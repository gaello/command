﻿using UnityEngine;

/// <summary>
/// Command that moves player in provided direction.
/// </summary>
public class MoveCommand : Command
{
    // Reference to the player movement.
    public PlayerMovement player;

    // Direction value.
    public Vector2Int direction;

    public override void Execute()
    {
        player.MovePlayer(direction);
    }
}
