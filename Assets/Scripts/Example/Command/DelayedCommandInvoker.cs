﻿using UnityEngine;

/// <summary>
/// Command Invoker which has a delay in command execution.
/// </summary>
public class DelayedCommandInvoker : CommandInvoker
{
    [SerializeField]
    private float commandsPerSecond = 1;

    private float lastExecution;
    private bool executeCommands = false;

    /// <summary>
    /// Method used to execute all commands from the buffer.
    /// </summary>
    public override void ExecuteBuffer()
    {
        executeCommands = true;
    }

    /// <summary>
    /// Unity method called each frame.
    /// </summary>
    private void Update()
    {
        // Determing if can execute commands.
        if (executeCommands && commandBuffer.Count > 0)
        {
            // If can check if there enough time passed by to execute next command.
            var diff = Time.time - lastExecution;
            if (diff / commandsPerSecond >= 1)
            {
                // Executing next command.
                lastExecution = Time.time;
                var c = commandBuffer.Dequeue();
                c.Execute();

                // If there is no commands left, stop trying to execute them.
                executeCommands = commandBuffer.Count != 0;
            }
        }
    }
}
