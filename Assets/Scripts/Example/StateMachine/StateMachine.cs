﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// State Machine implementation.
/// Uses BaseState as base class for storing currently operating state.
/// </summary>
public class StateMachine : MonoBehaviour
{
    // Reference to currently operating state.
    private BaseState currentState;

    [SerializeField]
    private MapGenerator mapGenerator;
    public MapGenerator MapGenerator => mapGenerator;

    [SerializeField]
    private PlayerMovement player;
    public PlayerMovement Player => player;

    [SerializeField]
    private PlayerDestination destination;
    public PlayerDestination Destination => destination;

    [SerializeField]
    private DelayedCommandInvoker commandInvoker;
    public DelayedCommandInvoker CommandInvoker => commandInvoker;

    [SerializeField]
    private UIRoot ui;
    public UIRoot UI => ui;

    /// <summary>
    /// Unity method called on a first frame as object is enabled.
    /// On start we are changing state so our ship will start patrolling.
    /// </summary>
    private void Start()
    {
        // You can create new state as here
        ChangeState(new PrepareLevelState());
    }

    /// <summary>
    /// Unity method called each frame
    /// </summary>
    private void Update()
    {
        // If we have reference to state, we should update it!
        if (currentState != null)
        {
            currentState.UpdateState();
        }
    }

    /// <summary>
    /// Method used to change state
    /// </summary>
    /// <param name="newState">New state.</param>
    public void ChangeState(BaseState newState)
    {
        // If we currently have state, we need to destroy it!
        if (currentState != null)
        {
            currentState.DestroyState();
        }

        // Swap reference
        currentState = newState;

        // If we passed reference to new state, we should assign owner of that state and initialize it!
        // If we decided to pass null as new state, nothing will happened.
        if (currentState != null)
        {
            currentState.owner = this;
            currentState.PrepareState();
        }
    }
}
