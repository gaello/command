﻿using UnityEngine;

/// <summary>
/// Game state in which we are collecting input commands from the UI.
/// </summary>
public class CommandCollectState : BaseState
{
    public override void PrepareState()
    {
        base.PrepareState();

        // Attaching events to the UI buttons.
        owner.UI.CollectCommandsView.OnExecuteClicked += OnExecuteClicked;

        owner.UI.CollectCommandsView.OnUpClicked += OnUpClicked;
        owner.UI.CollectCommandsView.OnDownClicked += OnDownClicked;
        owner.UI.CollectCommandsView.OnLeftClicked += OnLeftClicked;
        owner.UI.CollectCommandsView.OnRightClicked += OnRightClicked;

        owner.UI.CollectCommandsView.ShowView();
    }

    public override void DestroyState()
    {
        owner.UI.CollectCommandsView.HideView();

        // Detaching events from the UI buttons.
        owner.UI.CollectCommandsView.OnExecuteClicked -= OnExecuteClicked;

        owner.UI.CollectCommandsView.OnUpClicked -= OnUpClicked;
        owner.UI.CollectCommandsView.OnDownClicked -= OnDownClicked;
        owner.UI.CollectCommandsView.OnLeftClicked -= OnLeftClicked;
        owner.UI.CollectCommandsView.OnRightClicked -= OnRightClicked;

        base.DestroyState();
    }

    /// <summary>
    /// Method attached to the Execution button.
    /// Starts changes game state to the Execution State.
    /// </summary>
    private void OnExecuteClicked()
    {
        owner.ChangeState(new ExecutionState());
    }

    /// <summary>
    /// Method attached to the Up button.
    /// Adds move command to the buffer and updates command list in the UI.
    /// </summary>
    private void OnUpClicked()
    {
        owner.CommandInvoker.AddCommand(new MoveCommand() { direction = Vector2Int.up, player = owner.Player });
        owner.UI.CollectCommandsView.AddCommand("UP");
    }

    /// <summary>
    /// Method attached to the Down button.
    /// Adds move command to the buffer and updates command list in the UI.
    /// </summary>
    private void OnDownClicked()
    {
        owner.CommandInvoker.AddCommand(new MoveCommand() { direction = Vector2Int.down, player = owner.Player });
        owner.UI.CollectCommandsView.AddCommand("DOWN");
    }

    /// <summary>
    /// Method attached to the Left button.
    /// Adds move command to the buffer and updates command list in the UI.
    /// </summary>
    private void OnLeftClicked()
    {
        owner.CommandInvoker.AddCommand(new MoveCommand() { direction = Vector2Int.left, player = owner.Player });
        owner.UI.CollectCommandsView.AddCommand("LEFT");
    }

    /// <summary>
    /// Method attached to the Right button.
    /// Adds move command to the buffer and updates command list in the UI.
    /// </summary>
    private void OnRightClicked()
    {
        owner.CommandInvoker.AddCommand(new MoveCommand() { direction = Vector2Int.right, player = owner.Player });
        owner.UI.CollectCommandsView.AddCommand("RIGHT");
    }

}

