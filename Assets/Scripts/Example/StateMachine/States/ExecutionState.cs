﻿using UnityEngine;

/// <summary>
/// Execution state in which all commands are executed.
/// </summary>
public class ExecutionState : BaseState
{
    public override void PrepareState()
    {
        base.PrepareState();

        owner.CommandInvoker.ExecuteBuffer();
    }

    public override void UpdateState()
    {
        base.UpdateState();

        // State work as long as there are commands in the buffer
        if (!owner.CommandInvoker.HasCommandsInBuffer())
        {
            var gameOver = new GameOverState();
            // check if player is at the same position as destination.
            gameOver.won = owner.Player.CurrentPosition == owner.Destination.CurrentPosition;

            owner.ChangeState(gameOver);
        }
    }
}
