﻿using UnityEngine;

/// <summary>
/// Prepare level state. Generates map and place player and destination on the map.
/// </summary>
public class PrepareLevelState : BaseState
{
    public override void PrepareState()
    {
        base.PrepareState();

        // Map generating
        owner.MapGenerator.GenerateMap();

        // Positioning player in the center of the map.
        owner.Player.CurrentPosition = new Vector2Int(owner.MapGenerator.MapSize.x / 2, owner.MapGenerator.MapSize.y / 2);

        // Placing destination on the map. Guard for placing the destination at the same position as the player.
        do
        {
            owner.Destination.CurrentPosition = new Vector2Int(Random.Range(0, owner.MapGenerator.MapSize.x), Random.Range(0, owner.MapGenerator.MapSize.y));
        } while (owner.Player.CurrentPosition == owner.Destination.CurrentPosition);

        // Continuing to the collecting commands.
        owner.ChangeState(new CommandCollectState());
    }
}
