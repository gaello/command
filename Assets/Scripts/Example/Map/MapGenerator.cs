﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class generates and handles map.
/// </summary>
public class MapGenerator : MonoBehaviour
{
    [Header("Map")]
    // Reference to map tile.
    [SerializeField]
    private GameObject mapTilePrefab;

    // Map size.
    [SerializeField]
    private Vector2Int mapSize = new Vector2Int(5, 5);
    public Vector2Int MapSize => mapSize;

    // Distance between tiles.
    [SerializeField]
    private float tileOffset = 3.0f;

    // List of generated tiles.
    private List<GameObject> mapTiles = new List<GameObject>();

    /// <summary>
    /// Method called to generate map.
    /// </summary>
    public void GenerateMap()
    {
        for (int i = 0; i < mapSize.y; i++)
        {
            for (int j = 0; j < mapSize.x; j++)
            {
                var inst = Instantiate(mapTilePrefab, transform);
                inst.transform.localPosition = GetTilePosition(j, i);
                inst.transform.localRotation = Quaternion.identity;

                mapTiles.Add(inst);
            }
        }
    }

    /// <summary>
    /// Method which return position on tile base on X, Y value.
    /// </summary>
    /// <returns>The tile position.</returns>
    /// <param name="x">The x coordinate.</param>
    /// <param name="y">The y coordinate.</param>
    public Vector3 GetTilePosition(int x, int y)
    {
        return new Vector3(x - (mapSize.x / 2.0f), 0, y - (mapSize.y / 2.0f)) * tileOffset;
    }

    /// <summary>
    /// Method that returns map tile.
    /// </summary>
    /// <returns>The tile.</returns>
    /// <param name="x">The x coordinate.</param>
    /// <param name="y">The y coordinate.</param>
    public GameObject GetTile(int x, int y)
    {
        x = Mathf.Max(0, Mathf.Min(mapSize.x - 1, x));
        y = Mathf.Max(0, Mathf.Min(mapSize.y - 1, y));

        return mapTiles[y * mapSize.x + x];
    }

    /// <summary>
    /// Method which clamp Vector2Int value to the map size.
    /// </summary>
    /// <returns>The tiles.</returns>
    /// <param name="index">Index.</param>
    public Vector2Int ClampTiles(Vector2Int index)
    {
        index.x = Mathf.Max(0, Mathf.Min(mapSize.x - 1, index.x));
        index.y = Mathf.Max(0, Mathf.Min(mapSize.y - 1, index.y));

        return index;
    }
}
