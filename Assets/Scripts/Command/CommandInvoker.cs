﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Command invoker. Collects command into buffer to execute them at once.
/// </summary>
public class CommandInvoker : MonoBehaviour
{
    // Collected commands.
    protected Queue<Command> commandBuffer = new Queue<Command>();

    /// <summary>
    /// Method used to add new command to the buffer.
    /// </summary>
    /// <param name="command">New command.</param>
    public virtual void AddCommand(Command command)
    {
        commandBuffer.Enqueue(command);
    }

    /// <summary>
    /// This method tells if there is any command waiting in the buffer.
    /// </summary>
    /// <returns><c>true</c>, if buffer has some commands, <c>false</c> otherwise.</returns>
    public bool HasCommandsInBuffer()
    {
        return commandBuffer.Count > 0;
    }

    /// <summary>
    /// Method used to execute all commands from the buffer.
    /// </summary>
    public virtual void ExecuteBuffer()
    {
        foreach (var c in commandBuffer)
        {
            c.Execute();
        }

        ClearBuffer();
    }

    /// <summary>
    /// Method used to clear command buffer.
    /// </summary>
    public virtual void ClearBuffer()
    {
        commandBuffer.Clear();
    }
}
